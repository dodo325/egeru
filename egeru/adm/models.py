from django.db                  import models
from django.utils               import timezone

import pandas as pd
from lxml import html
import requests
from bs4 import BeautifulSoup
from random import choice, uniform
from time import sleep
def get_html(url, useragent=None, proxy=None):
    
    
    print('get_html')
    r = requests.get(url, headers=useragent, proxies=proxy)
    r.encoding = 'utf-8'
    return r.text


class Application(models.Model):
    author = models.ForeignKey('egeru_auth.User', on_delete=models.CASCADE,related_name='author', verbose_name="Автор")
    data = models.TextField(blank=True, null=True, help_text="Дополнительные информация",verbose_name="О нём")
    url = models.TextField(blank=True, null=True, help_text="Ссылка на список")
    name = models.TextField(blank=True, null=True, help_text="имя для поиска")
    
    max_apps =  models.PositiveSmallIntegerField(default=10,verbose_name="Количество мест")

    TYPE_CHOICES = (
        ('spbu', 'СПбГУ'),
        ('adm', 'admlist'),
    )
    type_content = models.CharField(max_length=6,
                                 choices=TYPE_CHOICES,
                                 default='adm',
                                 verbose_name="Источник"
                                 )

    def __str__(self):
        return str(self.pk) + ': '+ self.name

    class Meta:
        verbose_name_plural = "Заявки"
    

    n1 =  models.PositiveSmallIntegerField(default=0, help_text="Автоматически", verbose_name="Номер в ранжированном списке")
    n2 =  models.PositiveSmallIntegerField(default=0, help_text="Автоматически", verbose_name="Номер по оригиналам")
    n3 =  models.PositiveSmallIntegerField(default=0, help_text="Автоматически", verbose_name="Номер  с надеждой на то, что, все кто подал оригинал на другую программу, пойдут на неё")
    def parse(self):
        url = self.url
        df = None
        ht1 = None
        try:
            #useragents = open('useragents.txt').read().split('\n')
        #  useragent = {'User-Agent': choice(useragents)}
            ht1 = get_html(url,  {'User-Agent': 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0; chromeframe/11.0.696.57)'})
        except Exception as e:
            print("error: ", e)
        soup = BeautifulSoup(ht1, 'lxml')
        if self.type_content == 'adm':
            table = soup.findAll('table')[1]
            stable = str(table)
            df = pd.read_html(stable)[0]
            raw = df[df['ФИО'] == self.name]
            self.n1, self.n2, self.n3 = int(raw['№']), int(raw['№*']), int(raw['№**'])
    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('adm_user')