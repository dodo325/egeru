from django.contrib.auth.decorators import login_required
from django.http                    import HttpResponse
from django.template                import loader

from markdown2                      import Markdown
from .models        import Application
# Create your views here.



def adm_all(request):
    t = loader.get_template("adm/adm_all.html")

    c = {'apps':Application.objects.all()}
    return HttpResponse(t.render(c, request), content_type='text/html')

@login_required
def adm_user(request):
    t = loader.get_template("adm/adm_all.html")

    c = {'apps':Application.objects.filter(author=request.user)}
    return HttpResponse(t.render(c, request), content_type='text/html')