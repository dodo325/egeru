from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include, re_path

from .auth.views import account_profile
from .views import member_index, member_action

from egeru.adm.views import adm_all, adm_user
from egeru.main.views import problem_list, source_list, source_detail, problem_detail
urlpatterns = [
    # Landing page area
    re_path(r'^$', TemplateView.as_view(template_name='visitor/landing-index.html'), name='landing_index'),
    re_path(r'^about$', TemplateView.as_view(template_name='visitor/landing-about.html'), name='landing_about'),
    re_path(r'^terms/$', TemplateView.as_view(template_name='visitor/terms.html'), name='website_terms'),
    re_path(r'^contact$', TemplateView.as_view(template_name='visitor/contact.html'), name='website_contact'),

    # Account management is done by allauth
    re_path(r'^accounts/', include('allauth.urls')),

    # Account profile and member info done locally
    re_path(r'^accounts/profile/$', account_profile, name='account_profile'),
    re_path(r'^member/$', member_index, name='user_home'),
    re_path(r'^member/action$', member_action, name='user_action'),

    # main
    path('main/problem/list', problem_list, name='problem_list'),
    path('main/source/list', source_list, name='source_list'),
    path('main/source/<int:pk>', source_detail, name='source_detail'),
    path('main/problem/<int:pk>', problem_detail, name='problem_detail'),

    # adm
    path('adm/user', adm_user, name='adm_user'),
    path('adm/all', adm_all, name='adm_all'),


    # Usual Django admin
    re_path(r'^admin/', admin.site.urls, name='admin_site'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
