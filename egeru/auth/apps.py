from django.apps import AppConfig


class EgeRuAuthAppConfig(AppConfig):
    label = "egeru_auth"
    name = "egeru.auth"
    verbose_name = "EgeRu Auth"
