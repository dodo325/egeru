from django.contrib import admin

from .models        import Author, Problem, Source, Argument, MetaProblem

class AuthorAdmin(admin.ModelAdmin):
    ordering        = ('name',)
    search_fields   = ('name','data')

class ProblemAdmin(admin.ModelAdmin):
    ordering        = ('title',)
    search_fields   = ('title','data')

class SourceAdmin(admin.ModelAdmin):
    ordering        = ('title',)
    search_fields   = ('title','description')
    list_display    = ('title', 'author',)

admin.site.register(Author, AuthorAdmin)
admin.site.register(Problem, ProblemAdmin)
admin.site.register(Source, SourceAdmin)
admin.site.register(Argument)
admin.site.register(MetaProblem)