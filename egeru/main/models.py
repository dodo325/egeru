from django.db                  import models
from django.utils               import timezone
class Author(models.Model):
    name = models.CharField(max_length=200, verbose_name="Имя")
    data = models.TextField(blank=True, null=True, help_text="Дополнительные информация",verbose_name="О нём")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Авторы"

class MetaProblem(models.Model):
    title = models.CharField(max_length=200, verbose_name="Название метапроблемы") #
    data = models.TextField(blank=True, null=True, help_text="Дополнительные рекомендации",verbose_name="Рекомендации")
    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Метапроблемы"

class Problem(models.Model):
    title = models.CharField(max_length=200, verbose_name="Название проблемы") #
    data = models.TextField(blank=True, null=True, help_text="Дополнительные рекомендации",verbose_name="Рекомендации")

    metaProblem = models.ForeignKey(MetaProblem, blank=True, null=True,on_delete=models.CASCADE,related_name='metaProblem', verbose_name="Автор")
    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Проблемы"
class Source(models.Model):
    title = models.CharField(max_length=200, verbose_name="Название источника")
    author = models.ForeignKey(Author, on_delete=models.CASCADE,related_name='author', verbose_name="Автор")
    description = models.TextField(blank=True, null=True, help_text="Про текст", verbose_name="Описание") # описание markdown

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Источники"

class Argument(models.Model): # Аргумент к проблеме...
    problem = models.ForeignKey(Problem, on_delete=models.CASCADE,related_name='problem_arg')
    source = models.ForeignKey(Source, on_delete=models.CASCADE,related_name='problem_source', help_text="литаратура или опыт", verbose_name="Источник")
    description = models.TextField(blank=True, null=True, help_text="Это конкретный пример из текста", verbose_name="Пример") # описание markdown

    def __str__(self):
        return "{} -> {}".format(self.problem,self.source)

    class Meta:
        verbose_name_plural = "Аргументы"




