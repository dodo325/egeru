from django.contrib.auth.decorators import login_required
from django.http                    import HttpResponse
from django.template                import loader

from .models import Problem, Argument, Author, Source, MetaProblem

from markdown2                      import Markdown
# Create your views here.

@login_required
def problem_list(request):
    t = loader.get_template('main/problem-list.html')
    problem_list = Problem.objects.filter(metaProblem=None)
    li = { i:[j for j in i.metaProblem.all()] for i in MetaProblem.objects.all()}

    c = {'li':li, 'problem_list':problem_list}

    return HttpResponse(t.render(c, request), content_type='text/html')

@login_required
def source_list(request):

    t = loader.get_template('main/source-list.html')
    source_list = Source.objects.all()
    c = {'source_list':source_list}
    return HttpResponse(t.render(c, request), content_type='text/html')

@login_required
def source_detail(request, pk):
    t = loader.get_template('main/source.html')
    source = Source.objects.get(pk=pk)
    arguments = Argument.objects.filter(source=source)

    problem_list = [arg.problem for arg in arguments]

    # source.description
    markdowner = Markdown()
    raw_txt = markdowner.convert(source.description)

    c = {'source':source, 'page':raw_txt,'problem_list':problem_list}
    return HttpResponse(t.render(c, request), content_type='text/html')

@login_required
def problem_detail(request, pk):
    t = loader.get_template('main/problem.html')
    problem = Problem.objects.get(pk=pk)

    markdowner = Markdown()
    raw_txt = markdowner.convert(problem.data)

    c = {'problem':problem,
         'page':raw_txt,
         'arg_list':Argument.objects.filter(problem=problem)}
    return HttpResponse(t.render(c, request), content_type='text/html')
